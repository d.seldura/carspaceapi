import { StyleSheet } from 'react-native';
export default layout = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#6f2d91',
  },
  iconContainer: { flex: 1, alignSelf: 'center', paddingTop: 150 },
  input: {
    flex: 1,
    width: '60%',
    padding: 10,
    height: 20,
    borderBottomWidth: 2,
    borderBottomColor: 'white',
    fontSize: 18,
    color: '#fff',
  },
  inputContainer: { flex: 1, alignItems: 'center' },
  register: {
    display: 'flex',
    flex: 1,
    paddingTop: 30,
    fontSize: 20,
    alignSelf: 'center',
    color: '#fff',
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'center',
    marginTop: 50,
    color: '#fff',
    backgroundColor: '#6f2d91',
    width: 300,
    height: 100,
  },
});
