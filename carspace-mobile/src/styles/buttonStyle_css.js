import { StyleSheet } from 'react-native';
export default CustomButtonStyle = StyleSheet.create({
  button: {
    alignSelf: 'center',
    borderWidth: 3,
    borderColor: '#ffffff',
    borderRadius: 45,
    backgroundColor: '#6f2d91',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.39,
    shadowRadius: 8.3,
    elevation: 13,
    width: '75%',
    height: '150%',
  },
  buttonFont: {
    color: '#62FAC2',
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
});
