import 'react-native-gesture-handler';
import React, { useState } from './node_modules/react';
import { NavigationContainer } from './node_modules/@react-navigation/native';
import { createStackNavigator } from './node_modules/@react-navigation/stack';
import { Text, TextInput, View, StyleSheet } from 'react-native';
import CarSpaceLogo from './src/components/CarSpaceLogo';
import Custom_Button from './src/components/Custom_Button';
import getAPI from './src/env/Env';
import StorageService from './src/services/StorageService';

const Stack = createStackNavigator();
let SS = new StorageService();

export default function App() {
  SS.setData('potato', ['potato1', 'potato2']);
  console.log(SS.getData('potato'));
  return (
    <NavigationContainer>
      <View style={layout.container}>
        <View style={layout.iconContainer}>
          <CarSpaceLogo />
        </View>
        <InputComponent style={{ flex: 1 }}></InputComponent>
        <Text
          style={layout.register}
          accessibilityRole="link"
          onPress={() => console.log('SIGNUP')}
        >
          Sign Up
        </Text>
      </View>
    </NavigationContainer>
  );
}

const useField = (defaultValue) => {
  const [value, setValue] = useState(defaultValue);

  const handleChange = (e) => {
    setValue(e.nativeEvent.text);
  };

  return {
    value,
    setValue,
    handleChange,
  };
};

const InputComponent = () => {
  const email = useField('email');
  const password = useField('password');
  const login = (email, password) => {
    var payload = JSON.stringify({
      email: email,
      password: password,
    });
    //console.log(email, password, payload);
    fetch(getAPI() + '/users/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: payload,
    })
      .then((response) => response.json())
      .then((response) => {
        if (response.access_token) console.log('LOGGED IN SUCCESSFULLY');
        else {
          console.log('LOGIN FAILED');
          console.log(response);
        }
      });
  };

  return (
    <View style={layout.inputContainer}>
      <TextInput
        keyboardType="email-address"
        style={layout.input}
        name="email"
        value={email.value}
        onChange={email.handleChange}
      />
      <TextInput
        keyboardType="default"
        style={layout.input}
        name="password"
        secureTextEntry={true}
        value={password.value}
        onChange={password.handleChange}
      />
      <View style={layout.buttonContainer}>
        <Custom_Button
          label="LOGIN"
          styles={buttonStyle}
          onPress={() => login(email.value, password.value)}
        />
      </View>
    </View>
  );
};

const layout = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#6f2d91',
  },
  iconContainer: { flex: 1, alignSelf: 'center', paddingTop: 150 },
  input: {
    flex: 1,
    width: '60%',
    padding: 10,
    height: 20,
    borderBottomWidth: 2,
    borderBottomColor: 'white',
    fontSize: 18,
    color: '#fff',
  },
  inputContainer: { flex: 1, alignItems: 'center' },
  register: {
    display: 'flex',
    flex: 1,
    paddingTop: 30,
    fontSize: 20,
    alignSelf: 'center',
    color: '#fff',
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'center',
    marginTop: 50,
    color: '#fff',
    backgroundColor: '#6f2d91',
    width: 300,
    height: 100,
  },
});

const buttonStyle = StyleSheet.create({
  button: {
    alignSelf: 'center',
    borderWidth: 3,
    borderColor: '#ffffff',
    borderRadius: 45,
    backgroundColor: '#6f2d91',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.39,
    shadowRadius: 8.3,
    elevation: 13,
    width: '75%',
    height: '150%',
  },
  buttonFont: {
    color: '#62FAC2',
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
});
