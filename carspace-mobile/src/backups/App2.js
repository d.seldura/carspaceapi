import React, { Component, useState } from './node_modules/react';
import { Text, TextInput, View, StyleSheet, Button, Image } from 'react-native';

export default class App extends Component {
  render() {
    return (
      <View style={layout.container}>
        <View style={layout.iconContainer}>
          <Image
            style={{ width: 169, height: 120 }}
            source={{ uri: 'https://i.ibb.co/jWHybq7/Cs-Icon-No-BG.png' }}
          />
        </View>
        <InputComponent style={{ flex: 1 }}></InputComponent>
        <Text style={layout.register}>Sign Up</Text>
      </View>
    );
  }
}

const useField = (defaultValue) => {
  const [value, setValue] = useState(defaultValue);

  const handleChange = (e) => {
    //console.log(e.nativeEvent.text);
    setValue(e.nativeEvent.text);
  };

  return {
    value,
    setValue,
    handleChange,
  };
};

const InputComponent = () => {
  const email = useField('email');
  const password = useField('password');
  const result = useField('');
  const login = (email, password) => {
    var payload = JSON.stringify({
      email: email,
      password: password,
    });
    //console.log(email, password, payload);
    fetch('http://8fdb446a.ngrok.io/users/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: payload,
    })
      .then((response) => response.json())
      .then((response) => {
        if (response.access_token) console.log('LOGGED IN SUCCESSFULLY');
        else {
          console.log('LOGIN FAILED');
          console.log(response);
        }
      });
  };

  return (
    <View style={layout.inputContainer}>
      <TextInput
        keyboardType="email-address"
        style={layout.input}
        name="email"
        value={email.value}
        onChange={email.handleChange}
      />
      <TextInput
        keyboardType="default"
        style={layout.input}
        name="password"
        value={password.value}
        onChange={password.handleChange}
      />
      <View style={layout.button}>
        <Button title="Login" onPress={() => login(email.value, password.value)}></Button>
      </View>
    </View>
  );
};

const layout = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#6f2d91',
  },
  iconContainer: { flex: 1, alignSelf: 'center', paddingTop: 150 },
  input: {
    flex: 1,
    width: '60%',
    padding: 10,
    height: 20,
    borderBottomWidth: 2,
    borderBottomColor: 'white',
    fontSize: 18,
    color: '#fff',
  },
  inputContainer: { flex: 1, alignItems: 'center' },
  register: {
    display: 'flex',
    flex: 1,
    paddingTop: 25,
    fontSize: 20,
    alignSelf: 'center',
    color: '#fff',
  },
  button: {
    flex: 1,
    marginTop: 50,
    fontSize: 20,
    alignSelf: 'center',
    color: '#fff',
    backgroundColor: '#6f2d91',
  },
});
