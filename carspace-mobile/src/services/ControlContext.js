import React, { Component } from 'react';

const ControlContext = React.createContext();

class ControlProvider extends Component {
  // Context state
  state = {
    control: { isLoggedIn: false, isRegistered: false, token: '', ControlID: '' },
  };

  // Method to update state
  setControl = (control) => {
    this.setState((prevState) => ({ control }));
  };

  render() {
    const { children } = this.props;
    const { control } = this.state;
    const { setControl } = this;

    return (
      <ControlContext.Provider
        value={{
          control,
          setControl,
        }}
      >
        {children}
      </ControlContext.Provider>
    );
  }
}
const ControlConsumer = ControlContext.Consumer;
export default ControlContext;

export { ControlProvider, ControlConsumer };
