import getAPI from './../env/Env';

var response_data = '';
const setResponse = (data) => {
  response_data = data;
};

const login = (email, password) => {
  var payload = JSON.stringify({
    email: email,
    password: password,
  });

  fetch(getAPI() + '/users/login', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: payload,
  })
    .then((response) => response.json())
    .then((response) => {
      if (response.access_token) {
        setResponse(response.access_token);
      } else {
        setResponse(response.message);
      }
    });
  return response_data;
};

const register = async (data) => {
  var payload = JSON.stringify(data);
  await fetch(getAPI() + '/users/register', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: payload,
  })
    .then((response) => response.json())
    .then((response) => {
      if (response._id) {
        setResponse(response._id);
      } else {
        setResponse(response.message);
      }
    });
  return response_data;
};

export { login, register };
