export default class StorageService {
  constructor() {
    this.data = {};
  }

  setData(identifier, data) {
    this.data[identifier] = data;
  }

  getData(identifier) {
    return this.data[identifier];
  }

  vardump() {
    return this.data;
  }
}
