import React from 'react';
import { View, Image } from 'react-native';

export default () => {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Image
        style={{ width: 127, height: 90 }}
        source={{ uri: 'https://i.ibb.co/jWHybq7/Cs-Icon-No-BG.png' }}
      />
    </View>
  );
};
