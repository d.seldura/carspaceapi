import { Text, View, StyleSheet } from 'react-native';
import CarSpaceLogo from '../../CarSpaceLogo';
import { register } from '../../../services/APICalls';

import * as yup from 'yup';
import { Formik } from 'formik';
import React, { Fragment } from 'react';
import { TextInput } from 'react-native';
import Custom_Button from '../../Custom_Button';

const registerSchema = yup.object().shape({
  firstName: yup
    .string()
    .min(3)
    .required(),
  lastName: yup
    .string()
    .min(2)
    .required(),
  email: yup
    .string()
    .email()
    .required(),
  phoneNumber: yup
    .string()
    .min(11)
    .required(),
  password: yup
    .string()
    .min(6)
    .required(),
});

const submitHandler = async (values) => {
  //DEBUG
  console.log(JSON.stringify(values));
  //DEBUG
  console.log(await register(values));
};

export default Register = ({ navigation }) => {
  const submitHandler = async (values) => {
    //DEBUG
    console.log(JSON.stringify(values));
    //DEBUG
    var successfulRegister = await register(values);
    console.log(successfulRegister);
    if (successfulRegister)
      Alert.alert(
        'Error',
        'Successful registration, please login',
        [{ text: 'OK', onPress: () => navigation.navigate('Login') }],
        {
          cancelable: false,
        },
      );
  };
  return (
    <View style={layout.container}>
      <CarSpaceLogo />
      <View style={{ flex: 2.5, alignItems: 'center' }}>
        <Formik
          initialValues={{ firstName: '', lastName: '', email: '', phoneNumber: '', password: '' }}
          onSubmit={submitHandler}
          validationSchema={registerSchema}
        >
          {({ values, handleChange, errors, setFieldTouched, touched, isValid, handleSubmit }) => (
            <Fragment>
              <TextInput
                style={input_style.input}
                value={values.firstName}
                onChangeText={handleChange('firstName')}
                onBlur={() => setFieldTouched('firstName')}
                placeholder="First Name"
              />
              {touched.firstName && errors.firstName && (
                <Text style={{ fontSize: 10, color: 'red' }}>{errors.firstName}</Text>
              )}
              <TextInput
                style={input_style.input}
                value={values.lastName}
                onChangeText={handleChange('lastName')}
                onBlur={() => setFieldTouched('lastName')}
                placeholder="Last Name"
              />
              {touched.lastName && errors.lastName && (
                <Text style={{ fontSize: 10, color: 'red' }}>{errors.lastName}</Text>
              )}
              <TextInput
                style={input_style.input}
                value={values.email}
                onChangeText={handleChange('email')}
                onBlur={() => setFieldTouched('email')}
                placeholder="E-mail"
                keyboardType="email-address"
              />
              {touched.email && errors.email && (
                <Text style={{ fontSize: 10, color: 'red' }}>{errors.email}</Text>
              )}
              <TextInput
                style={input_style.input}
                value={values.phoneNumber}
                onChangeText={handleChange('phoneNumber')}
                onBlur={() => setFieldTouched('phoneNumber')}
                placeholder="Phone Number"
              />
              {touched.phoneNumber && errors.phoneNumber && (
                <Text style={{ fontSize: 10, color: 'red' }}>{errors.phoneNumber}</Text>
              )}
              <TextInput
                style={input_style.input}
                value={values.password}
                onChangeText={handleChange('password')}
                placeholder="Password"
                onBlur={() => setFieldTouched('password')}
                secureTextEntry={true}
              />
              {touched.password && errors.password && (
                <Text style={{ fontSize: 10, color: 'red' }}>{errors.password}</Text>
              )}
              <Custom_Button
                label="Register"
                styles={buttonStyle}
                disabled={!isValid}
                onPress={handleSubmit}
              />
            </Fragment>
          )}
        </Formik>
      </View>
    </View>
  );
};

const buttonStyle = StyleSheet.create({
  button: {
    alignSelf: 'center',
    borderWidth: 3,
    borderColor: '#ffffff',
    borderRadius: 45,
    backgroundColor: '#6f2d91',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.39,
    shadowRadius: 8.3,
    elevation: 13,
    width: '60%',
    marginVertical: 20,
  },
  buttonFont: {
    color: '#62FAC2',
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
});

const input_style = StyleSheet.create({
  input: {
    width: '60%',
    paddingVertical: 5,
    borderBottomWidth: 2,
    borderBottomColor: 'white',
    fontSize: 18,
    color: '#fff',
  },
});

const layout = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#6f2d91',
  },
  register: {
    display: 'flex',
    flex: 1,
    fontSize: 20,
    alignSelf: 'center',
    color: '#fff',
  },
});
