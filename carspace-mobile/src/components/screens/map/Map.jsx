import React from 'react';
import { StyleSheet, View } from 'react-native';
import Constants from 'expo-constants';
import MapContainer from './MapContainer';

export default class Map extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <MapContainer />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
  },
});
