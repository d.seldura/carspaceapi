import React from 'react';
import { View } from 'react-native';
import MapInput from './MapInput';
import MyMapView from './MapView';
import { getLocation, geocodeLocationByName } from '../../../services/location-service';

class MapContainer extends React.Component {
  state = {
    region: {},
  };

  componentDidMount() {
    this.getInitialState();
  }

  getInitialState() {
    getLocation().then((data) => {
      console.log(data);
      this.setState({
        region: {
          latitude: data.latitude,
          longitude: data.longitude,
          latitudeDelta: 0.008,
          longitudeDelta: 0.008,
        },
      });
    });
  }

  getCoordsFromName(loc) {
    this.setState({
      region: {
        latitude: loc.lat,
        longitude: loc.lng,
        latitudeDelta: 0.008,
        longitudeDelta: 0.008,
      },
    });
  }

  onMapRegionChange(region) {
    this.setState({ region });
  }

  render() {
    return (
      <View style={{ flex: 1, display: 'flex' }}>
        <View style={{ flex: 1 }}>
          <MapInput notifyChange={(loc) => this.getCoordsFromName(loc)} />
        </View>

        {this.state.region['latitude'] ? (
          <View style={{ flex: 9 }}>
            <MyMapView
              region={this.state.region}
              onRegionChange={(reg) => this.onMapRegionChange(reg)}
            />
          </View>
        ) : null}
      </View>
    );
  }
}

export default MapContainer;
