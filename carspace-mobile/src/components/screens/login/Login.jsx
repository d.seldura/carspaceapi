import React, { Fragment } from 'react';
import { Text, View, StyleSheet, TextInput, Alert } from 'react-native';
import * as yup from 'yup';
import { Formik } from 'formik';

//custom components
import CarSpaceLogo from '../../CarSpaceLogo';
import Custom_Button from '../../Custom_Button';
//custom services
import { login } from './../../../services/APICalls';

const loginSchema = yup.object().shape({
  email: yup
    .string()
    .email()
    .required(),
  password: yup
    .string()
    .min(6)
    .required(),
});

export default Login = ({ navigation }) => {
  var isLoggedIn = false;
  const submitHandler = (values) => {
    //DEBUG
    console.log(JSON.stringify(values));
    //DEBUG
    var response = login(values.email, values.password);
    console.log(response);
    if (response === 'Invalid password or email') {
      isLoggedIn = false;
      Alert.alert(
        'Error',
        response,
        [{ text: 'OK', onPress: () => navigation.navigate('Login') }],
        {
          cancelable: false,
        },
      );
    } else {
      isLoggedIn = true;
      console.log(response);
    }
    if (isLoggedIn) {
      Alert.alert(
        'Login',
        'Logged in successfully',
        [{ text: 'OK', onPress: () => navigation.navigate('Home') }],
        { cancelable: false },
      );
    }
  };

  return (
    <View style={layout.container}>
      <CarSpaceLogo />
      <View style={{ flex: 1, alignItems: 'center' }}>
        <Formik
          initialValues={{ email: '', password: '' }}
          onSubmit={submitHandler}
          validationSchema={loginSchema}
        >
          {({ values, handleChange, errors, setFieldTouched, touched, isValid, handleSubmit }) => (
            <Fragment>
              <TextInput
                style={input_style.input}
                value={values.email}
                onChangeText={handleChange('email')}
                onBlur={() => setFieldTouched('email')}
                placeholder="E-mail"
                keyboardType="email-address"
              />
              {touched.email && errors.email && (
                <Text style={{ fontSize: 10, color: 'red' }}>{errors.email}</Text>
              )}
              <TextInput
                style={input_style.input}
                value={values.password}
                onChangeText={handleChange('password')}
                placeholder="Password"
                onBlur={() => setFieldTouched('password')}
                secureTextEntry={true}
              />
              {touched.password && errors.password && (
                <Text style={{ fontSize: 10, color: 'red' }}>{errors.password}</Text>
              )}
              <Custom_Button
                label="Sign In"
                styles={buttonStyle}
                disabled={!isValid}
                onPress={handleSubmit}
              />
            </Fragment>
          )}
        </Formik>
      </View>

      <Text
        style={layout.register}
        accessibilityRole="link"
        onPress={() => navigation.navigate('Register')}
      >
        Register
      </Text>
    </View>
  );
};

const buttonStyle = StyleSheet.create({
  button: {
    alignSelf: 'center',
    borderWidth: 3,
    borderColor: '#ffffff',
    borderRadius: 45,
    backgroundColor: '#6f2d91',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.39,
    shadowRadius: 8.3,
    elevation: 13,
    width: '60%',
    marginVertical: 20,
  },
  buttonFont: {
    color: '#62FAC2',
    fontSize: 30,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
});

const input_style = StyleSheet.create({
  input: {
    width: '60%',
    paddingVertical: 5,
    borderBottomWidth: 2,
    borderBottomColor: 'white',
    fontSize: 18,
    color: '#fff',
  },
});

const layout = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#6f2d91',
  },
  register: {
    display: 'flex',
    flex: 1,
    fontSize: 20,
    alignSelf: 'center',
    color: '#fff',
  },
});
