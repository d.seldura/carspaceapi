import React, { useState } from 'react';
import { TextInput, StyleSheet } from 'react-native';

export default InputComponent = (props) => {
  const useField = (defaultValue) => {
    const [value, setValue] = useState(defaultValue);
    const handleChange = (e) => {
      setValue(e.nativeEvent.text);
    };

    return {
      value,
      setValue,
      handleChange,
    };
  };

  const fieldData = useField(props.value);

  return (
    <TextInput
      keyboardType={props.keyboardType}
      secureTextEntry={props.secureTextEntry}
      style={input_style.input}
      name={props.value}
      value={fieldData.value}
      onChange={fieldData.handleChange}
    />
  );
};

const input_style = StyleSheet.create({
  input: {
    width: '60%',
    paddingVertical: 5,
    borderBottomWidth: 2,
    borderBottomColor: 'white',
    fontSize: 18,
    color: '#fff',
  },
});
