import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

export default (props) => {
  return (
    <TouchableOpacity style={props.styles.button} onPress={props.onPress}>
      <Text style={props.styles.buttonFont}>{props.label}</Text>
    </TouchableOpacity>
  );
};
