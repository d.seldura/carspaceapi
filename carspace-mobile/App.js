import React, { Component } from 'react';

import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

//CUSTOM IMPORTS
import Login from './src/components/screens/login/Login';
import Register from './src/components/screens/register/Register';
import Home from './src/components/screens/home/Home';
import { ControlProvider, ControlConsumer } from './src/services/ControlContext';

const Stack = createStackNavigator();

export default function App() {
  return (
    <ControlProvider>
      <Main />
    </ControlProvider>
  );
}

class Main extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="Home" component={Home} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
