/*
 Basic ESP8266 MQTT example

 This sketch demonstrates the capabilities of the pubsub library in combination
 with the ESP8266 board/library.

 It connects to an MQTT server then:
  - publishes "hello world" to the topic "outTopic" every two seconds
  - subscribes to the topic "inTopic", printing out any messages
    it receives. NB - it assumes the received payloads are strings not binary
  - If the first character of the topic "inTopic" is an 1, switch ON the ESP Led,
    else switch it off

 It will reconnect to the server if the connection is lost using a blocking
 reconnect function. See the 'mqtt_reconnect_nonblocking' example for how to
 achieve the same result without blocking the main loop.

 To install the ESP8266 board, (using Arduino 1.6.4+):
  - Add the following 3rd party board manager under "File -> Preferences -> Additional Boards Manager URLs":
       http://arduino.esp8266.com/stable/package_esp8266com_index.json
  - Open the "Tools -> Board -> Board Manager" and click install for the ESP8266"
  - Select your ESP8266 in "Tools -> Board"

*/

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Update these with values suitable for your network.

const char* ssid = "Tomas";
const char* password = "jesurythomas";
const char* mqtt_server = "192.168.43.168";

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value_01;
const int trigPin = 4; 
const int echoPin = 5;  
const int redPin01 = 13;
const int greenPin01 = 15;
long duration_01;
int distance_01;
int ledPin = 13;
int initialize = 0;
int prevState = 1; //0 = vacant 1 = occupied
int currState = 0;


void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      // ... and resubscribe
      client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  pinMode(greenPin01, OUTPUT);
  pinMode(redPin01, OUTPUT);
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();


    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW); 
    duration_01 = pulseIn(echoPin, HIGH);
    distance_01 = checkDistance(duration_01);



  if (initialize == 0)
  {
  initial_1(duration_01);

  }

   if (initialize > 0)
  {
  distance_01 = checkDistance(duration_01);

  currState = changeState(distance_01);

  if (prevState != currState)
  {
  if (currState == 0)
  {
    value_01 = 1;
    snprintf (msg, 50,"4:%ld", value_01);
    Serial.println("");
    Serial.println(distance_01);
    Serial.println(msg);
    client.publish("outTopic", msg);
    digitalWrite(redPin01, LOW);
    delay(100);
    digitalWrite(greenPin01, HIGH);

  }
  else
  {
    value_01 = 0;
   snprintf (msg, 50,"4:%ld", value_01);
    Serial.println("");
    Serial.println(distance_01);
    Serial.println(msg);
    client.publish("outTopic", msg);
    digitalWrite(greenPin01, LOW);
    delay(100);
    digitalWrite(redPin01, HIGH);
  }
  }
  }
  
  initialize++;
  prevState = currState;
  delay(2000);
  
  
}


int changeState (int distance)
{
  int change;
  
  if (distance <= 25 )
   change = 1;
   else
   change = 0;

   return change;
}


int checkDistance(long duration)
{
  int distance;

  distance = duration*0.034/2;

  return distance;
}



 void initial_1(long duration)
{
 int distance;
 distance = checkDistance(duration);
 prevState = changeState(distance);
 if (prevState == 0)
    {
    value_01 = 1;
    snprintf (msg, 50,"4:%ld", value_01);
    Serial.println("");
    Serial.println(distance_01);
    Serial.println(msg);
    client.publish("outTopic", msg);
    digitalWrite(redPin01, LOW);
    delay(100);
    digitalWrite(greenPin01, HIGH);

    }
   else
    {
    value_01 = 0;
    snprintf (msg, 50,"4:%ld", value_01);
    Serial.println("");
    Serial.println(distance_01);
    Serial.println(msg);
    client.publish("outTopic", msg);
    digitalWrite(greenPin01, LOW);
    delay(100);
    digitalWrite(redPin01, HIGH); 
    }
}
