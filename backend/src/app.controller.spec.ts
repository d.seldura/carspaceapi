import { Controller, Request, Post, UseGuards } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Controller()
export class AppController {
  @Post('auth/login')
  async login(@Request() req) {
    return req.user;
  }
}
