import { Injectable, NotFoundException, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './user.model';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  async addUser(
    firstName: string,
    lastName: string,
    email: string,
    phoneNumber: string,
    password: string,
  ): Promise<User> {
    const salt = bcrypt.genSaltSync(10);
    const hashedPassword = await bcrypt.hash(password, salt);
    const createdUser = new this.userModel({
      firstName,
      lastName,
      email,
      phoneNumber,
      password: hashedPassword,
    });
    const result = await createdUser.save();
    console.log('Result of creation');
    console.log(result);
    return result;
  }

  async getUsers(): Promise<User[]> {
    return this.userModel.find().exec();
  }

  async getOne(param: string): Promise<User> {
    return this.userModel
      .findOne(
        {
          email: param,
        },
        {
          _id: 1,
          email: 1,
          password: 1,
        },
      )
      .exec();
  }

  async login(username: string, pass: string): Promise<any> {
    const user = await this.getOne(username);
    if (user && (await bcrypt.compare(pass, user.password))) {
      const result = user;
      const payload = {
        result,
      };
      console.log('Password successful', result);
      return {
        access_token: jwt.sign(
          {
            data: result,
          },
          'thisIsATemporarySecretKeySoSHHSHHkaLang0208',
          { expiresIn: '1h' },
        ),
      };
    } else {
      throw new HttpException('Invalid password or email', HttpStatus.FORBIDDEN);
      return 'Login Failed';
    }
  }
}
