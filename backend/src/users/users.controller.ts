import { Controller, Post, Body, Get, Param, Req } from '@nestjs/common';
import { UsersService } from './users.service';
import { Request } from 'express';
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}
  //comment
  @Post('register')
  adduser(
    @Body('firstName') firstName: string,
    @Body('lastName') lastName: string,
    @Body('email') email: string,
    @Body('phoneNumber') phoneNumber: string,
    @Body('password') password: string,
  ) {
    const retVal = this.usersService.addUser(firstName, lastName, email, phoneNumber, password);
    return retVal;
  }

  @Post('login')
  login(@Body('email') email: string, @Body('password') password: string, @Req() request: Request) {
    console.log(request.body);
    const retVal = this.usersService.login(email, password);
    return retVal;
  }

  @Get()
  getAllusers() {
    return this.usersService.getUsers();
  }

  @Get('info')
  getuser(@Body('email') param: string) {
    return this.usersService.getOne(param);
  }
}
