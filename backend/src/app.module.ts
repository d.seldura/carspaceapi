import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { MongooseModule } from '@nestjs/mongoose';

const CloudConnect =
  'mongodb+srv://zenithdevgroup:tagay123@cluster0-cpt5v.gcp.mongodb.net/CarSpace?retryWrites=true&w=majority';
const LocalConnect = 'mongodb://127.0.0.1:27017/CarSpace';

@Module({
  imports: [UsersModule, MongooseModule.forRoot(LocalConnect)],
  controllers: [],
  providers: [],
})
export class AppModule {}
