import { User } from './user.model';
import { Model } from 'mongoose';
export declare class UsersService {
    private readonly userModel;
    constructor(userModel: Model<User>);
    addUser(firstName: string, lastName: string, email: string, phoneNumber: string, password: string): Promise<User>;
    getUsers(): Promise<User[]>;
    getOne(param: string): Promise<User>;
    login(username: string, pass: string): Promise<any>;
}
