import * as mongoose from 'mongoose';
export declare const ProductSchema: any;
export interface User extends mongoose.Document {
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    registrationDate: Date;
    password: string;
}
export declare const UserSchema: any;
