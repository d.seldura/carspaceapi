import { UsersService } from './users.service';
import { Request } from 'express';
export declare class UsersController {
    private readonly usersService;
    constructor(usersService: UsersService);
    adduser(firstName: string, lastName: string, email: string, phoneNumber: string, password: string): Promise<import("./user.model").User>;
    login(email: string, password: string, request: Request): Promise<any>;
    getAllusers(): Promise<import("./user.model").User[]>;
    getuser(param: string): Promise<import("./user.model").User>;
}
