"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
let UsersService = class UsersService {
    constructor(userModel) {
        this.userModel = userModel;
    }
    async addUser(firstName, lastName, email, phoneNumber, password) {
        const salt = bcrypt.genSaltSync(10);
        const hashedPassword = await bcrypt.hash(password, salt);
        const createdUser = new this.userModel({
            firstName,
            lastName,
            email,
            phoneNumber,
            password: hashedPassword,
        });
        const result = await createdUser.save();
        console.log('Result of creation');
        console.log(result);
        return result;
    }
    async getUsers() {
        return this.userModel.find().exec();
    }
    async getOne(param) {
        return this.userModel
            .findOne({
            email: param,
        }, {
            _id: 1,
            email: 1,
            password: 1,
        })
            .exec();
    }
    async login(username, pass) {
        const user = await this.getOne(username);
        if (user && (await bcrypt.compare(pass, user.password))) {
            const result = user;
            const payload = {
                result,
            };
            console.log('Password successful', result);
            return {
                access_token: jwt.sign({
                    data: result,
                }, 'thisIsATemporarySecretKeySoSHHSHHkaLang0208', { expiresIn: '1h' }),
            };
        }
        else {
            throw new common_1.HttpException('Invalid password or email', common_1.HttpStatus.FORBIDDEN);
            return 'Login Failed';
        }
    }
};
UsersService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('User')),
    __metadata("design:paramtypes", [typeof (_a = typeof mongoose_2.Model !== "undefined" && mongoose_2.Model) === "function" ? _a : Object])
], UsersService);
exports.UsersService = UsersService;
//# sourceMappingURL=users.service.js.map