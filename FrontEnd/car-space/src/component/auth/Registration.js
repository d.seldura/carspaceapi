import React , {Component} from 'react'
import { Form,Button ,Col} from 'react-bootstrap'
import axios from 'axios'
class Registration extends Component{
    constructor(props){
        super(props)
        this.state={
                        firstName: "",
                        lastName: "",
                        phoneNumber: "",
                        email: "",
                        password:"", 
                        registrationError: ""  
        }
        this.handleSubmit=this.handleSubmit.bind(this)
        this.handleChange= this.handleChange.bind(this)
        this.handleSuccessfulAuth = this.handleSuccessfulAuth.bind(this)
    }
    handleSuccessfulAuth(data)
    {
        //todo update parent component
        //this.props.handleLogin(data);
        this.props.history.push("/dashboard");//take all those props in the app and dont overwrite
  
    }
    handleChange(event){
        console.log("handle change", event)
        this.setState({
                [event.target.name]: event.target.value
        })
    }

    handleSubmit(event){
        console.log("Form Submit",{   user: {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            phoneNumber: this.state.phoneNumber,
            email: this.state.email,
            password: this.state.password

    }})
    const {firstName,lastName,phoneNumber,email,password} = this.state;
        axios.post("http://8fdb446a.ngrok.io/users",{        
            
                firstName: firstName,
                lastName: lastName,
                email: email,
                phoneNumber: phoneNumber,
                password: password
            
        
            }
            ,{withCredentials: true}
            ).then(response =>{
                console.log("response res", response)
               /* if(response.data.status === "created"){ } */ //this one will trigered if create and navigate to dashboard
          
                //this.props.handleSuccessfulAuth(response.data)
                this.handleSuccessfulAuth(response.data)
            
            }).catch(error=>{
                console.log("registration Error", error)
            })
        event.preventDefault();
    }
    render()
    {
        return(
            <div>

               {/* <Form style={{margin: "1%",width: "40%",paddingLeft: "10em"}} onSubmit={this.handleSubmit}> */}

               <Form style={{margin: "1%",width: "40%",paddingLeft: "10em"}} onSubmit={this.handleSubmit}>
  <Form.Row>
    <Form.Group as={Col} controlId="formFirstName">
      <Form.Label>First Name</Form.Label>
      <Form.Control type="text" 
      name="firstName"
      value={this.state.firstName}
      onChange={this.handleChange} required
      placeholder="First Name" />
    </Form.Group>

    <Form.Group as={Col} controlId="formLastName">
      <Form.Label>Last Name</Form.Label>
      <Form.Control type="text"  
      name="lastName"
      value={this.state.lastName}
      onChange={this.handleChange} required
      placeholder="Last Name" /> 
    </Form.Group>
  </Form.Row>

  <Form.Group controlId="formphoneNumber" >
        <Form.Label>Phone Number</Form.Label>
        <Form.Control type="text" 
            placeholder="Phone Number"  
            name="phoneNumber" 
            value={this.state.phoneNumber}          
            onChange={this.handleChange} required/>  
        </Form.Group>
    
        <Form.Row>
    <Form.Group as={Col} controlId="formEmail">
      <Form.Label>Email Address</Form.Label>
      <Form.Control type="email" 
      name="email"
      value={this.state.email}
      onChange={this.handleChange} required
      placeholder="Email Address" />
    </Form.Group>

    <Form.Group as={Col} controlId="formPassword">
      <Form.Label>Password</Form.Label>
      <Form.Control type="password"  
      name="password"
      value={this.state.password}
      onChange={this.handleChange} required
      placeholder="Password" /> 
    </Form.Group>
  </Form.Row>


 

  <Button style={{marginLeft: "40%"}} variant="primary" type="submit" >
      Register
  </Button>
</Form>



               {/* <form onSubmit={this.handleSubmit}>
                   <input type="text"
                            name="firstName"
                            placeholder="First Name"
                            value={this.state.firstName}
                            onChange={this.handleChange} required />

                            <input type="text"
                            name="lastName"
                            placeholder="Last Name"
                            value={this.state.lastName}
                            onChange={this.handleChange} required />
                            <br />
                            <input type="text"
                            name="phoneNumber"
                            placeholder="Phone Number"
                            value={this.state.phoneNumber}
                            onChange={this.handleChange} required />

                            <br />
                            <input type="email"
                            name="email"
                            placeholder="Email"
                            value={this.state.email}
                            onChange={this.handleChange} required />

                            <input type="text"
                            name="password"
                            placeholder="Password"
                            value={this.state.password}
                            onChange={this.handleChange} required />

                            <button type="submit">Register</button>
               </form>
               <h1>Your name is {this.state.firstName}</h1> */}
            </div>
        )
        
    }
}

export default Registration