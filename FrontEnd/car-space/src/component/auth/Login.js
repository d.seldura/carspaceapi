import React , {Component} from 'react'
import axios from 'axios'
import { Form,Button } from 'react-bootstrap'
class Login extends Component{
    constructor(props){
        super(props)
        this.state={
                        
                        email: "",
                        password:"", 
                        logInErrors: ""  
        }
        this.handleSubmit=this.handleSubmit.bind(this)
        this.handleChange= this.handleChange.bind(this)
        this.handleSuccessfulAuth=this.handleSuccessfulAuth.bind(this)

    }
    handleSuccessfulAuth(data)
    {
        //todo update parent component
        //this.props.handleLogin(data);
        this.props.history.push("/dashboard");//take all those props in the app and dont overwrite
  
    }

    handleChange(event){
        console.log("handle change", event)
        this.setState({
                [event.target.name]: event.target.value
        })
    }

    handleSubmit(event){
        console.log("Form Submit",{   user: {
            email: this.state.email,
            password: this.state.password

    }})
    const{email,password}=this.state;
        axios.post("http://8fdb446a.ngrok.io/users/login",{
             
                email: email,
                password: password

}
            
            ,{withCredentials: true}
            ).then(response =>{
                console.log("response res Login", response)
               /* if(response.data.status === "created"){ } */ //this one will trigered if create and navigate to dashboard
               //this.props.handleSuccessfulAuth(response.data)
                this.handleSuccessfulAuth(response.data)
            
            
            }).catch(error=>{
                console.log("registration Error", error)
            })
        event.preventDefault();
    }
    render()
    {
        return(
            <div>

<Form style={{margin: "1%",width: "40%",paddingLeft: "10em"}} onSubmit={this.handleSubmit}>

  <Form.Group controlId="formBasicEmail" >
    <Form.Label>Email address</Form.Label>
    <Form.Control type="email" placeholder="Enter email"  name="email"  value={this.state.email}
                            onChange={this.handleChange} required/>
 
  </Form.Group>

  <Form.Group controlId="formBasicPassword">
    <Form.Label>Password</Form.Label>
    <Form.Control type="password" placeholder="Password" name="password" value={this.state.password} onChange={this.handleChange} required/>
  </Form.Group>
 
  <Button style={{marginLeft: "40%"}} variant="primary" type="submit" >
    Login
  </Button>
</Form>


               {/* <form onSubmit={this.handleSubmit}>
           

                           
                            <input type="email"
                            name="email"
                            placeholder="Email"
                            value={this.state.email}
                            onChange={this.handleChange} required />

                            <input type="password"
                            name="password"
                            placeholder="Password"
                            value={this.state.password}
                            onChange={this.handleChange} required />

                            <button type="submit">Login</button>
               </form> */}
               
            </div>
        )
        
    }
}

export default Login