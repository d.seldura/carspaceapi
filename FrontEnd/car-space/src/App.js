import React, {Component} from 'react'
import {BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import { Navbar,Nav,Form, FormControl,Button } from 'react-bootstrap'
import Dashboard from './component/Dashboard';
import Home from './component/Home';
import 'bootstrap/dist/css/bootstrap.min.css';
import Login from './component/auth/Login'
import Registration from './component/auth/Registration';
// import Navigation from './component/auth/Navigation'
// import Registration from './component/auth/Registration'
// import Login from './component/auth/Login'

// class App extends Component {
//   constructor()
//     {  super()

//   }
//   render(){
//     return(
//       <BrowserRouter>
//           <div className="container">
//           <Navigation/>
//             <Switch>
//               <Route path='/' component={Home} exact/>
//               <Route path='/login' component={Login}/>
//               <Route path='/register' component={Registration}/>

//             </Switch>
//           </div>
//       </BrowserRouter>
//     )
//   }
// }
// export default App;



 class App extends Component {
  constructor(){
    super();

    this.state={
      loggedInStatus: "Not Logged In",
      user:{}
    }
    this.handleLogin=this.handleLogin.bind(this)
    this.routeChange = this.routeChange.bind(this)
  }

  routeChange() {
    this.props.history.push("/login");
  }
  handleLogin(data){
      this.setState({
        loggedInStatus: "Loggen In",
        user: data
      });
  }
 
  
  
     
  render()
  {
    return (
 
      <div className="app">
          
            
      

       
        <Router>

  

        <Navbar bg="dark" variant="dark">
    <Navbar.Brand href="/">Home</Navbar.Brand>
    <Nav className="mr-auto">
      <Nav.Link href="/login">Login</Nav.Link>
      <Nav.Link href="/register">Register</Nav.Link>
     
    </Nav>

  </Navbar>

        {/* <nav className="navbar" style={{backgroundColor: "#5f0a87"}}>
          
        <ul>
           <li> <Link to="/">Home</Link></li>
           <li> <Link to="/login">Login</Link></li>
           <li><Link to="/register">Register</Link></li>
          
        </ul>
      
           
            
            </nav> */}
        <Switch>
       
          {/* <Route 
          exact 
          path={"/"}
           render={props => (
            <Home {...props} handleLogin={this.handleLogin} loggedInStatus={this.state.loggedInStatus}/>
          )} 
          /> */}
       <Route 
          exact 
          path={"/dashboard"}
           render={props => (
            <Dashboard 
            {...props} 
            loggedInStatus={this.state.loggedInStatus}/>
          )} 
          />
             <Route 
          exact 
          path={"/"}
          component ={Home}
          />

     <Route 
          exact 
          path={"/login"}
          component ={Login}
          />

        <Route 
          exact 
          path={"/register"}
          component ={Registration}
          />
        </Switch>
        </Router>
       
      </div>
    );
  }
}

export default App;